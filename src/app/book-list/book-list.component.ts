import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BookService} from '../book.service';
import {Book} from '../book-item/book-item.component';
import {DialogService} from 'ng2-bootstrap-modal';
import {EditBookComponent} from '../edit-book/edit-book.component';
import {AddBookComponent} from '../add-book/add-book.component';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  books = new Map<number, Book>();
  private currentPage = 1;
  searchStr = '';

  constructor(private bookService: BookService, private dialogService: DialogService) {
  }

  getValuesArray(map: Map<any, any>): any[] {
    return Array.from(map.values());
  }


  ngOnInit() {
    this.bookService.getAllBooksObs().subscribe(data => {
      for (let i = 0; i < data.length; i++) {
        this.books.set(data [i].id, data[i]);
      }
    });
  }

  deleteBook(id: number) {
    this.bookService.deleteBookObs(id).subscribe(
      data => {
        this.books.delete(id);
      },
      e => {
        console.log('Error. Can not delete book');
      });
  }

  markBookAsRead(id: number) {
    this.bookService.markBookAsReadObs(id).subscribe(
      data => {
        const book = this.books.get(id);
        book.readAlready = true;
        this.books.set(id, book);
      },
      e => {
        console.log('Error. Can not mark book as read');
      }
    );
  }

  scrollToFirst(page: number) {
    window.location.href = '#book-list';
  }

  readSearchStr(text: string) {
    this.searchStr = text;
  }

  resetSearchStr() {
    this.searchStr = '';
  }

  editBook(book: Book) {
    const disposable = this.dialogService.addDialog(EditBookComponent, book)
      .subscribe((updatedBook: Book) => {
        if (updatedBook !== undefined)
          this.bookService.updateBookObs(updatedBook).subscribe(
            (next: Book) => {
              if (JSON.stringify(next) !== JSON.stringify(updatedBook)) {
                console.log('updated on client not equals updated on server');
                console.log('client' + JSON.stringify(updatedBook));
                console.log('server' + JSON.stringify(next));
              }
              this.books.set(updatedBook.id, next);
            },
            err => {
              console.log('update error');
            },
          );
      });
  }

  addBook() {
    this.dialogService.addDialog(AddBookComponent)
      .subscribe((book: Book) => {
        if (book !== undefined) {
          this.bookService.addBook(book).subscribe(
            (next: Book) => {
              if (book.author !== next.author
                || book.title !== next.title
                || book.description !== next.description
                || book.printYear !== next.printYear
                || book.readAlready !== next.readAlready
                || book.isbn !== next.isbn) {
                console.log('added book on client not equals added book on server');
                console.log('client' + JSON.stringify(book));
                console.log('server' + JSON.stringify(next));
              }
              this.books.set(next.id, next);
            },
            e => {
              console.log('add error');
            }
          );
        }
      });
  }
}
