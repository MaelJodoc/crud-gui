import {Component, Input, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {Book} from '../book-item/book-item.component';


@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent extends DialogComponent<Book, Book> implements Book, OnInit {
  id: number;
  title: string;
  description: string;
  author: string;
  isbn: string;
  readAlready = false;
  printYear: number;

  @Input() newTitle: string;
  @Input() newDescription: string;
  @Input() newIsbn: string;
  @Input() newPrintYear: number;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }


  ngOnInit(): void {
    this.newTitle = this.title;
    this.newDescription = this.description;
    this.newIsbn = this.isbn;
    this.newPrintYear = this.printYear;
  }

  updateTitle(event) {
    const text: string = event.target.value.trim();
    if (text.length === 0) this.newTitle = this.title;
    else this.newTitle = text;
  }

  updateDescription(event) {
    const text = event.target.value.trim();
    if (text.length === 0) this.newDescription = this.description;
    else this.newDescription = text;
  }

  updateYear(event) {
    const text = event.target.value.trim();
    if (text.length === 0) this.newPrintYear = this.printYear;
    else {
      this.newPrintYear = parseInt(text, 10);
      if (isNaN(this.newPrintYear)) this.newPrintYear = this.printYear;
    }
  }

  updateIsbn(event) {
    const text = event.target.value.trim();
    if (text.length === 0) this.newIsbn = this.isbn;
    else this.newIsbn = text;
  }

  confirm() {
    this.result = {
      id: this.id,
      title: this.newTitle,
      description: this.newDescription,
      author: this.author,
      isbn: this.newIsbn,
      printYear: this.newPrintYear,
      readAlready: false,
    };
    this.close();
  }
}
