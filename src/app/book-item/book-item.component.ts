import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BookService} from '../book.service';

export interface Book {
  id: number;
  title: string;
  description: string;
  author: string;
  isbn: string;
  readAlready: boolean;
  printYear: number;
}

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.css']
})


export class BookItemComponent implements OnInit {
  @Input() book: Book;
  @Output() deleteBookEvent = new EventEmitter<number>();
  @Output() markAsReadEvent = new EventEmitter<number>();
  @Output() editBookEvent = new EventEmitter<Book>();

  constructor(private bookService: BookService) {
  }


  ngOnInit(): void {
  }

  markAsRead(id: number) {
    this.markAsReadEvent.emit(id);
  }

  deleteBook(id: number) {
    this.deleteBookEvent.emit(id);
  }

  editBook(book: Book) {
    this.editBookEvent.emit(book);
  }
}
