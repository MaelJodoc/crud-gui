import {Component, Input, OnInit} from '@angular/core';
import {DialogComponent, DialogService} from 'ng2-bootstrap-modal';
import {Book} from '../book-item/book-item.component';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent extends DialogComponent<any, Book> implements Book {
  id = 0;
  title = '';
  description = '';
  author = '';
  isbn = '';
  readAlready = false;

  @Input() printYear = 0;

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm() {
    this.result = {
      id: 0,
      title: this.title,
      description: this.description,
      author: this.author,
      isbn: this.isbn,
      printYear: this.printYear,
      readAlready: this.readAlready,
    };
    this.close();
  }

  setPrintYear(event) {
    this.printYear = parseInt(event.target.value.trim(), 10);
    if (isNaN(this.printYear)) this.printYear = 0;
  }
}
