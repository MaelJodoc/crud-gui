import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {BookItemComponent} from './book-item/book-item.component';
import {BookListComponent} from './book-list/book-list.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BookService} from './book.service';
import {NgxPaginationModule} from 'ngx-pagination';
import {SearchPipe} from './search.pipe';
import {SearchFormComponent} from './search-form/search-form.component';
import {BootstrapModalModule} from 'ng2-bootstrap-modal';
import {EditBookComponent} from './edit-book/edit-book.component';
import { AddBookComponent } from './add-book/add-book.component';


@NgModule({
  declarations: [
    AppComponent,
    BookItemComponent,
    BookListComponent,
    SearchPipe,
    SearchFormComponent,
    EditBookComponent,
    AddBookComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    BootstrapModalModule
  ],
  entryComponents: [
    EditBookComponent,
    AddBookComponent
  ],
  providers: [BookService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
