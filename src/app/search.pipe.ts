import {Pipe, PipeTransform} from '@angular/core';
import {Book} from './book-item/book-item.component';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(books: Book[], title: string): Book[] {
    return books.filter(book => {
      return book.title.toLowerCase().includes(title.toLowerCase());
    });
  }
}
