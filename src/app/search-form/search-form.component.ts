import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  @Output() changeTextEvent = new EventEmitter<string>();
  @Output() resetEvent = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  onChangeText(event) {
    this.changeTextEvent.emit(event.target.value);
  }

  onReset(event) {
    this.resetEvent.emit();
  }
}
