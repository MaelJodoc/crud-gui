import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Book} from './book-item/book-item.component';


@Injectable()
export class BookService implements OnInit {

  private pageSize = 10;
  private projectName = 'crud-0.1/';
  private url = 'http://localhost:8080/' + this.projectName + 'books/';
  private booksCount: number;

  ngOnInit(): void {
  }

  constructor(private httpClient: HttpClient) {
  }

  getAllBooksObs(): Observable<Book[]> {
    return this.httpClient.request<Book[]>('GET', this.url + 'getAll');
  }

  getPageObs(page: number, size: number = this.pageSize): Observable<Book[]> {
    return this.httpClient.request<Book[]>('GET', this.url + 'get/?page=' + page + '&size=' + size);
  }

  initAndGetBooksCount(): number {
    this.httpClient.request<number>('GET', this.url + 'count').subscribe(data => this.booksCount);
    return this.booksCount;
  }

  markBookAsReadObs(id: number): Observable<Object> {
    return this.httpClient.request('PUT', this.url + 'setRead', {
      body: id
    });
  }

  deleteBookObs(id: number): Observable<Object> {
    return this.httpClient.request('DELETE', this.url + 'delete', {
      body: id
    });
  }

  updateBookObs(updatedBook: Book): Observable<Object> {
    return this.httpClient.request('PUT', this.url + 'update', {
      body: updatedBook
    });
  }

  addBook(book: Book): Observable<Object> {
    return this.httpClient.post(this.url + 'add', book);
  }
}
